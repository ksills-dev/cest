#pragma once

/** @mainpage
# C'est La Vie
TODO: Basic Description

## Using C'est
TODO: Example

### Groups

### Environment Sharing
TODO: Describe how to modify environment & inter-test locals.

### Command Line Flags

### Customizing Output
<table>
<caption id=""></caption>
<tr><th>Definition <th>Description <th> Default
<tr><td> CEST_TF_RESET     <td> The terminal sequence used to reset output
                                formatting.
                           <td> CSI 0m (ANSI style Reset)
<tr><td> CEST_TF_EMPH     <td> The terminal sequence used before showing the
                                title card (for emphasis).
                           <td> CSI 1m (Bold)
<tr><td> CEST_TF_GOOD      <td> The terminal sequence used before reporting
                                test success.
                           <td> CSI 32m ()
<tr><td> CEST_TF_GOOD      <td> The terminal sequence used before reporting
                                test warnings.
                           <td> CSI 33m ()
<tr><td> CEST_TF_BAD       <td> The terminal sequence used before reporting
                                test failure.
                           <td> CSI 31m (red)
<tr><td> CEST_BULLET       <td> The character(s) printed as the bullet for a
                                test case line.
                           <td> "◉"
<tr><td> CEST_HORIZ_BORDER <td> The character(s) representing a single segment
                                in a horizontal border. If multiple visual
                                glyphs are provided, the length of the border
                                will be n times longer than the title card.
                           <td> "━"
<tr><td> CEST_INDENT_GUIDE <td> The character(s) representing a single layer in
                                the visual indentation guide.
                           <td> "│ "
</table>

## Building C'est
TODO: Meson

## Implementation Details
For those who want the power to abuse or modify, we'll also outline the
operating mechanics of c'est's implementation.

TODO: Summarize how it works
**/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*──────────────────────────────────────────────────────────────────────────────
Sinful Defines
────────────────────────────────────────────────────────────────────────────────
*/

#ifndef CEST_TF_RESET
#define CEST_TF_RESET "\033[0m"
#endif

#ifndef CEST_TF_EMPH
#define CEST_TF_EMPH "\033[1m"
#endif

#ifndef CEST_TF_GOOD
#define CEST_TF_GOOD "\033[32m"
#endif

#ifndef CEST_TF_IFFY
#define CEST_TF_IFFY "\033[33m"
#endif

#ifndef CEST_TF_BAD
#define CEST_TF_BAD "\033[31m"
#endif

#ifndef CEST_BULLET
#define CEST_BULLET "◉"
#endif

#ifndef CEST_HORIZ_BORDER
#define CEST_HORIZ_BORDER "━"
#endif

#ifndef CEST_INDENT_GUIDE
#define CEST_INDENT_GUIDE "│ "
#endif

#ifndef CEST_INDENT_BEGIN
#define CEST_INDENT_BEGIN "┌┨ "
#endif

#ifndef CEST_INDENT_END
#define CEST_INDENT_END "└╼ "
#endif

/*──────────────────────────────────────────────────────────────────────────────
C'Est La Vie Internals
────────────────────────────────────────────────────────────────────────────────
*/

typedef struct CestEnv {
  char const *suite_name;
  bool use_colors;
  size_t level;
  size_t count;
  size_t failed;
  size_t warned;
  size_t passed;
} CestEnv;

typedef enum CestResult {
  Cest_Pass, ///< The test passed.
  Cest_Warn, ///< The test encountered an unrelated problem.
  Cest_Fail  ///< The test failed.
} CestResult;

typedef struct CestContext {
} CestContext;

static char const cest_help[] = //
    "-- C'Est La Vie Generated Test Suite --\n"
    "Running this executable will perform the defined tests for the %s suite.\n"
    "\n"
    "Optional arguments:\n"
    "\t-[n]c      | [Do not] use colors in output.\n"
    "\t-h         | Print this help.\n";

/**
 * @brief Parse the command line arguments and initialize a new test environment
 * with the appropriate values.
 *
 * @param[in] suite_name The name of the test suite.
 * @param[in] argc The number of command line arguments.
 * @param[in] argv The value of the command line arguments.
 * @return A new CestEnv with the appropriately filled members.
 *
 * @note Will not return (exit code 1) if bad arguments are passed in.
 * @note Will not return (exit code 0) if the help argument is found.
 *
 * @see CestEnv
 *
 * @internal
 */
CestEnv cest_create_test_env(char *const suite_name, int argc,
                             char const **argv) {
  CestEnv env = {.suite_name = suite_name,
                 .use_colors = true,
                 .level = 0,
                 .count = 0,
                 .failed = 0,
                 .warned = 0,
                 .passed = 0};

  for (size_t i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
      printf("Bad argument form @ \"%s\"\n", argv[i]);
      exit(1);
    }

    bool negate = (argv[i][1] == 'n') ? true : false;
    size_t j = (negate) ? 2 : 1;
    switch (argv[i][j]) {
    case 'c':
      if (argv[i][j + 1]) {
        goto DEFAULT;
      }
      env.use_colors = !negate;
      break;

    case 'h':
      if (negate) {
        goto DEFAULT;
      }
      printf(cest_help, suite_name);
      exit(0);
      break;

    default:
    DEFAULT:
      printf("No such argument %s", argv[i]);
      exit(1);
      break;
    }
  }

  return env;
}

void print_border(size_t count) {
  for (size_t i = 0; i < count; i++) {
    printf(CEST_HORIZ_BORDER);
  }
  putchar('\n');
}

void print_indent_guide(size_t count) {
  for (size_t i = 0; i < count; i++) {
    printf(CEST_INDENT_GUIDE);
  }
}

CestContext get_context(CestEnv env) {
  CestContext result = {};
  return result;
}

#define TEST_LAMBDA(name, body)                                                \
  CestResult name(CestContext *context) {                                      \
    CestResult result = Cest_Pass;                                             \
    body;                                                                      \
    return result;                                                             \
  };

  /*────────────────────────────────────────────────────────────────────────────
  C'Est La Vie Interface
  ────────────────────────────────────────────────────────────────────────────*/

#define CEST_LA_VIE(name, children)                                            \
  int main(int argc, char const **argv) {                                      \
    printf("Initializing test suite.\n");                                      \
    CestEnv env = cest_create_test_env(#name, argc, argv);                     \
                                                                               \
    char const intro[] = #name " Test Suite";                                  \
    size_t border_size = strlen(intro);                                        \
    print_border(border_size);                                                 \
    printf("%2$s%1$s%3$s\n", intro, (env.use_colors) ? CEST_TF_EMPH : "",      \
           (env.use_colors) ? CEST_TF_RESET : "");                             \
    print_border(border_size);                                                 \
                                                                               \
    {children};                                                                \
                                                                               \
    print_border(border_size);                                                 \
    if (env.use_colors) {                                                      \
      printf(CEST_TF_EMPH "Executed: " CEST_TF_RESET "%zd\n" /**/              \
             CEST_TF_GOOD "Passed: " CEST_TF_RESET "%zd\n"   /**/              \
             CEST_TF_IFFY "Warned: " CEST_TF_RESET "%zd\n"   /**/              \
             CEST_TF_BAD "Failed: " CEST_TF_RESET "%zd\n",                     \
             env.count, env.passed, env.warned, env.failed);                   \
    } else {                                                                   \
      printf("Executed: %zd\n"                                                 \
             "Passed: %zd\n"                                                   \
             "Warned: %zd\n"                                                   \
             "Failed: %zd\n",                                                  \
             env.count, env.passed, env.warned, env.failed);                   \
    }                                                                          \
    return 0;                                                                  \
  }

#define CEST_GROUP(name, children)                                             \
  {                                                                            \
    size_t initial_count = env.count;                                          \
    size_t initial_passed = env.passed;                                        \
    size_t initial_warned = env.warned;                                        \
    size_t initial_failed = env.failed;                                        \
    print_indent_guide(env.level);                                             \
    printf(CEST_INDENT_BEGIN "%s\n", #name);                                   \
    env.level += 1;                                                            \
    {children};                                                                \
    env.level -= 1;                                                            \
    print_indent_guide(env.level);                                             \
                                                                               \
    size_t counted = env.count - initial_count;                                \
    size_t passed = env.passed - initial_passed;                               \
    size_t warned = env.warned - initial_warned;                               \
    size_t failed = env.failed - initial_failed;                               \
    size_t accounted_for = 0;                                                  \
                                                                               \
    printf(CEST_INDENT_END);                                                   \
    printf("%s%zd%s = ", (env.use_colors) ? CEST_TF_EMPH : "",                 \
           env.count - initial_count, (env.use_colors) ? CEST_TF_RESET : "");  \
                                                                               \
    if (passed) {                                                              \
      accounted_for += passed;                                                 \
      printf("%s%zd%s", (env.use_colors) ? CEST_TF_GOOD : "", passed,          \
             (env.use_colors) ? CEST_TF_RESET : "");                           \
      if (accounted_for != counted) {                                          \
        printf(" & ");                                                         \
      }                                                                        \
    }                                                                          \
    if (warned) {                                                              \
      accounted_for += warned;                                                 \
      printf("%s%zd%s", (env.use_colors) ? CEST_TF_IFFY : "", warned,          \
             (env.use_colors) ? CEST_TF_RESET : "");                           \
      if (accounted_for != counted) {                                          \
        printf(" & ");                                                         \
      }                                                                        \
    }                                                                          \
    if (failed) {                                                              \
      accounted_for += failed;                                                 \
      printf("%s%zd%s", (env.use_colors) ? CEST_TF_BAD : "", failed,           \
             (env.use_colors) ? CEST_TF_RESET : "");                           \
    }                                                                          \
    putchar('\n');                                                             \
    print_indent_guide(env.level);                                             \
    putchar('\n');                                                             \
  }

#define CEST_TEST(name, body)                                                  \
  {                                                                            \
    print_indent_guide(env.level);                                             \
    printf(CEST_BULLET " %s... ", #name);                                      \
    CestContext context = get_context(env);                                    \
    CestResult result;                                                         \
    TEST_LAMBDA(name, body)                                                    \
    result = name(&context);                                                   \
    env.count += 1;                                                            \
    switch (result) {                                                          \
    case Cest_Pass:                                                            \
      env.passed += 1;                                                         \
      printf("%sPASSED%s\n", (env.use_colors) ? CEST_TF_GOOD : "",             \
             (env.use_colors) ? CEST_TF_RESET : "");                           \
      break;                                                                   \
    case Cest_Warn:                                                            \
      env.warned += 1;                                                         \
      printf("%sWARNED%s\n", (env.use_colors) ? CEST_TF_IFFY : "",             \
             (env.use_colors) ? CEST_TF_RESET : "");                           \
      break;                                                                   \
    case Cest_Fail:                                                            \
      env.failed += 1;                                                         \
      printf("%sFAILED%s\n", (env.use_colors) ? CEST_TF_BAD : "",              \
             (env.use_colors) ? CEST_TF_RESET : "");                           \
      break;                                                                   \
    }                                                                          \
  }
